; Initializes the filament change.
;; https://marlinfw.org/docs/gcode/M600.html
;; https://marlinfw.org/docs/gcode/M300.html

; play tune
M300 S440 P200
M300 S660 P250
M300 S880 P300
; initialize filament change
M600
; Plays tone and pauses the print at the last location and waits till button pressed.
;; https://marlinfw.org/docs/gcode/M300.html
;; https://marlinfw.org/docs/gcode/M000-M001.html

; play tune
M300 S440 P200
M300 S660 P250
M300 S880 P300
; display message
M0 Click to resume
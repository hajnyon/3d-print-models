$fn = 100;

width = 44;
length = 86;
height = 6;

printerResolution = 0.4;

module base() {
	hull() {
		translate([0, 0, 0]) circle(1);
		translate([0, length/3, 0]) circle(1);
		translate([width, 0, 0]) circle(1);
		translate([width, length/3, 0]) circle(1);
		translate([width/4 + width/2, length/3 + length/2, 0]) circle(1);
		translate([width/4, length/3 + length/2, 0]) circle(1);
		square([width, length/3], center=false);
		translate([width/4, length/3, 0])
			square([width/2, length/2], center=false);
	}
}

holeLength = 22;
holeWidth = 34;
holeOffset = 10;
lengthRemainder = (holeWidth - holeLength) / 2;
module hole() {
	polygon([[0,0], [holeWidth,0], [holeWidth-lengthRemainder, holeLength], [lengthRemainder, holeLength]]);
}

coinDiameter = 20;
coinOverlap = 4;
coinHeight = 2.2;
module coinHole() {
	linear_extrude(coinHeight)
		square([coinDiameter, coinDiameter - coinOverlap]);
}


difference() {
	linear_extrude(height) {
		difference() {
			base();
			translate([width / 9, holeOffset, 0])
				hole();
		}
	}
	translate([width / 2 - coinDiameter / 2, holeLength + holeOffset - printerResolution, height / 2 - coinHeight / 2])
		coinHole();
}
	/*
minkowski() {
	linear_extrude(height)		
		difference() {
			base();
			translate([width/9, 10, 0])
				hole();
		}
	linear_extrude(height)
		circle(1);
}

*/
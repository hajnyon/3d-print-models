$fn = 50;

cr = 2.5; // circle radius
cd = cr * 2; // circle diameter
h = 30; // hight
w = 132; // width
hr = 5.9; // hole radius
hs = 110; // hole spacing

linear_extrude(5) {
    difference() {
        hull() {
            translate([cr, cr, 0]) {
                circle(d=cd);
            }

            translate([h - cr, cr, 0]) {
                circle(d=cd);
            }
            translate([h - cr, w - cr, 0]) {
                circle(d=cd);
            }

            translate([cr, w - cr, 0]) {
                circle(d=cd);
            }
        }
        translate([18, 6 + hr, 0]) {
            circle(hr);
        }
        translate([18, 6 + hr + hs, 0]) {
            circle(hr);
        }
        translate([35, 66, 0]) {
            resize([35,85]) {
                circle(d=50);
            }
        }
    }
    
}
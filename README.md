# 3D print models

My models for 3D printing modeled in [OpenSCAD](https://openscad.org/) and [onshape](https://cad.onshape.com).

Photos and print settings in [Printables profile](https://www.printables.com/@hajnyon_192244/models).
